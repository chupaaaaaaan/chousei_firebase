# インストールとサインアップ

ハンズオン開催までにお願いしたい事前準備がいくつかあります(ソフトウェアのインストール、サービスへのサインアップ)。

すでに開発をされている方はほとんど手間がかからないと思います。
まだの方はこの機会にがんばって環境を整えてみてください。

各プロダクトのインストール手順は示しませんが、公式ドキュメントに従ってインストールしていただければOKです。
どうしてもわからない場合は当日サポートしますが、ハンズオンの時間を有効活用するため、できるだけ準備をお願いします。

## Git

[Git](https://git-scm.com/)をインストールしておいてください。


## Node.js

[Node.js](https://nodejs.org/ja/)のVersion12(LTS)を利用できるようにしておいてください。

インストール方法はインストーラーでもnvm,nodebrew等でも構いません。

``` sh
$ node --version
v12.18.0
```

## テキストエディタ

JavaScriptのコーディング用のテキストエディタとして[Visual Studio Code](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)を用意しておいてください。  
今回はハンズオンのサポート時にVisual Studio Codeの[LiveShare](https://docs.microsoft.com/ja-jp/visualstudio/liveshare/)機能を利用します。  

1. [Visual Studio Code](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)をインストールしてください。  

2. Visual Studio Live Share拡張機能を利用するため、[Micosoft](https://support.microsoft.com/ja-jp/help/4026324/microsoft-account-how-to-create)か[Github](https://github.com/)のアカウントを作成してください。(既にアカウントを持っている場合はこの手順をスキップしてください)  

3. インストールしたVisual Stduio Codeを開き、マーケットプレースから Visual Studio Live Share 拡張機能をダウンロードしてインストールします。

4. Visual Stduio Codeを再起動し、依存関係がダウンロードおよびインストールされるまで待機します (ステータス バーを参照)。

5. 再起動すると画面下部にLive Shareとと書かれたリンクが表示されるので、それをクリックしてMicosoftかGithubのアカウントでログインしておいてください。  


## Googleアカウント

Googleのアカウントを作成しておいてください。
Firebaseの利用に必要となります。

https://support.google.com/accounts/answer/27441?hl=ja


## Firebase

### Firebaseプロジェクトの作成
ハンズオンで使用するFirebaseプロジェクトを作成します。

[Firebaseプロジェクトを作成する](https://firebase.google.com/docs/web/setup?hl=ja#create-project)を参照してください。

プロジェクト名は`chousei-firebase`にしてください。プロジェクト名を決めると、プロジェクト名を元に一意のプロジェクトIDが採番されます。

プロジェクトIDは、下記のようにFirebase Hostingのサブドメインで使用されます。

`https://<プロジェクトID>.firebaseapp.com`

プロジェクトIDは後から変更できないので、もし変更したい場合はプロジェクトを新しく作ってください。

### Firebase CLIのインストール

[Firebase CLI リファレンスの「設定」](https://firebase.google.com/docs/cli/?hl=ja#setup)の手順に従って、Firebase CLIをインストールし、ログインまでしてみましょう。

`+  Success! Logged in as xxxxxx@gmail.com`
とコンソールに表示されれば、ログインが成功しています。

もし、`firebase`というコマンドが見つからない場合、npmのパスが通っていないためにグローバルインストールしたコマンド(firebase)を使えない可能性があります。
その場合は、下記コマンドの結果をパスに追加してください。

`npm bin -g`

### Firebaseプロジェクトにアプリを登録する
Firebaseプロジェクトのコンソールから、アプリを登録します。

まだ一つもアプリを登録していない場合は、Overviewに「開始するにはアプリを追加してください」と記載があるので、その上に並ぶアイコンの中から`</>`を選んでください。

これでWebアプリを登録することができます。

アプリ登録画面で、ニックネームに`chousei-firebase`を指定して「アプリを登録」を押下しましょう。

この段階では、Hostingの設定はしなくて結構です。

## SCM, CD/CI(GitLab)

[GitLab](https://about.gitlab.com/)のアカウントを作成しておいてください。

Google,Twitter,GitHub等のアカウントでサインインすることも可能です。


## あったほうがいいもの(任意)

### Chrome

開発には任意のブラウザを使用いただけますが、
[Chrome](https://www.google.com/intl/ja_ALL/chrome/)があると、トラブルシューティングやデバッグ等に便利です。

Visual Studio Codeをお使いの場合は、[Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)を
インストールすると簡単にVSCode上でデバッガが使用できるのでおすすめです。


## Visual Studio Live Shareの利用方法

1. Live Share機能を有効化するためMicrosoftかGithubのアカウントでログインすると、画面に下部にログインユーザー名が表示されます。そのユーザー名をクリックしてください。  

2. ログインユーザー名をクリックすると、画面上部にCopy Link書かれた案内が表示されるので、その案内をクリックしてください。
クリックすると共有用のリンクが作成されます。  

3. 共同でコードを操作したい相手にリンクを連携してください。  

4. ターミナルの操作が必要な場合は手順1. と同じユーザー名をクリックして、表示されたShare Terminalと書かれたリンクをクリックしてください。  