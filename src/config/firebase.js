import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAJPRvl74I-1Adpa3WcfbjIHyAibMmKQyI",
  authDomain: "chousei-firebase-871d7.firebaseapp.com",
  databaseURL: "https://chousei-firebase-871d7.firebaseio.com",
  projectId: "chousei-firebase-871d7",
  storageBucket: "chousei-firebase-871d7.appspot.com",
  messagingSenderId: "772517060297",
  appId: "1:772517060297:web:886da5c385f8bab909f718",
  measurementId: "G-VSSPGT1DZ6"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
